#!/usr/bin/env python
# encoding: utf-8

import numpy as np
from scipy.io import loadmat
from scipy import optimize
import matplotlib.pyplot as plt


def estimate_gaussian(X):
    m, n = X.shape
    mu = (1/m) * sum(X)
    sigma_2 = (1/m) * sum((X - mu) ** 2)
    return mu, sigma_2


def multivariate_gaussian(X, mu, sigma_2):
    k = mu.size

    if sigma_2.ndim == 1:
        sigma_2 = np.diag(sigma_2)

    X = X - mu
    p = (2 * np.pi) ** (- k / 2) * np.linalg.det(sigma_2) ** (-0.5)\
        * np.exp(-0.5 * np.sum(np.dot(X, np.linalg.pinv(sigma_2)) * X, axis=1))

    return p


def select_threshold(y_val, p_val):
    best_epsilon = 0
    best_F1 = 0

    for epsilon in np.linspace(1.01*min(p_val), max(p_val), 1000):
        predictions = p_val < epsilon

        true_positives = np.sum((predictions == 1) & (y_val == 1))
        false_positives = np.sum((predictions == 1) & (y_val == 0))
        false_negatives = np.sum((predictions == 0) & (y_val == 1))

        precision = true_positives / (true_positives + false_positives)
        recall = true_positives / (true_positives + false_negatives)

        F1 = (2 * precision * recall) / (precision + recall)

        if F1 > best_F1:
            best_F1 = F1
            best_epsilon = epsilon

    return best_epsilon, best_F1


def cost_function(params, Y, R, num_users, num_movies, num_features,
                  lambda_=0.0):

    X = params[:num_movies*num_features].reshape(num_movies, num_features)
    theta = params[num_movies*num_features:].reshape(num_users, num_features)

    # Cost function
    error = (X @ np.transpose(theta)) - Y
    cost_regularisation = (lambda_ / 2) * np.sum(theta ** 2)\
        + (lambda_ / 2) * np.sum(X ** 2)
    J = 1/2 * np.sum((error ** 2) * R) + cost_regularisation

    # Gradient
    X_grad = (error * R) @ theta + lambda_ * X
    theta_grad = np.transpose(error * R) @ X + lambda_ * theta

    grad = np.concatenate([X_grad.ravel(), theta_grad.ravel()])
    return J, grad


def normalise_ratings(Y, R):
    m, n = Y.shape
    Y_mean = np.zeros(m)
    Y_norm = np.zeros(Y.shape)

    for i in range(m):
        idx = R[i, :] == 1
        Y_mean[i] = np.mean(Y[i, idx])
        Y_norm[i, idx] = Y[i, idx] - Y_mean[i]

    return Y_norm, Y_mean


def load_movie_list():
    with open(("movie_ids.txt"), encoding='ISO-8859-1') as fid:
        movies = fid.readlines()

    movie_names = []
    for movie in movies:
        parts = movie.split()
        movie_names.append(' '.join(parts[1:]).strip())

    return movie_names


def visualise_fit(X, mu, sigma_2, outliers=None):
    X1, X2 = np.meshgrid(np.arange(0, 35.5, 0.5), np.arange(0, 35.5, 0.5))
    Z = multivariate_gaussian(np.stack([X1.ravel(), X2.ravel()], axis=1),
                              mu, sigma_2)
    Z = Z.reshape(X1.shape)

    plt.plot(X[:, 0], X[:, 1], 'bx', mec='b', mew=2, ms=3)

    if np.all(abs(Z) != np.inf):
        plt.contour(X1, X2, Z, levels=10**(np.arange(-20., 1, 3)), zorder=100)

    if outliers is not None:
        plt.plot(X[outliers, 0], X[outliers, 1], 'ro', ms=10, mfc='None',
                 mew=2)

    plt.show()


def plot_data(X, y, xlabel, ylabel, title):
    plt.plot(X, y, 'bx', ms=3)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.show()


if __name__ == "__main__":

    print("---------------------")
    print("- Anomaly detection -")
    print("---------------------")
    print("\n")

    # Import data
    data = loadmat("ex8data1.mat")
    X, X_val, y_val = data['X'], data['Xval'], data['yval'][:, 0]

    # Visualise data
    plot_data(X[:, 0], X[:, 1], "Latency (ms)", "Through (mb/s)", "Ex8 Data 1")

    # Estimate parameters for a Gaussian
    mu, sigma_2 = estimate_gaussian(X)
    p = multivariate_gaussian(X, mu, sigma_2)
    visualise_fit(X, mu, sigma_2)

    # Select the threshold
    p_val = multivariate_gaussian(X_val, mu, sigma_2)
    epsilon, F1 = select_threshold(y_val, p_val)
    print(f"Using cross validation set:\n best epislon: {epsilon}\n best F1: {F1}\n")

    # Visualise anomalies
    outliers = p < epsilon
    visualise_fit(X, mu, sigma_2, outliers)

    # High dimensional dataset
    print("-- High dimensional dataset --")

    # Import data
    data = loadmat("ex8data2.mat")
    X, X_val, y_val = data['X'], data['Xval'], data['yval'][:, 0]

    mu, sigma_2 = estimate_gaussian(X)
    p = multivariate_gaussian(X, mu, sigma_2)
    p_val = multivariate_gaussian(X_val, mu, sigma_2)
    epsilon, F1 = select_threshold(y_val, p_val)
    number_outliers = np.sum(p < epsilon)

    print(f"Using cross validation set:\n best epislon: {epsilon}\n best F1: {F1}\n")
    print(f"Number of outliers: {number_outliers}\n")

    print("----------------------")
    print("- Recommender system -")
    print("----------------------")
    print("\n")

    # Import data
    data = loadmat("ex8_movies.mat")
    Y, R = data['Y'], data['R']

    data = loadmat("ex8_movieParams.mat")
    X, theta, num_users, num_movies, num_features = data['X'], \
        data['Theta'], data['num_users'], data['num_movies'], \
        data['num_features']

    # Example mean for Movie 1
    print(f"Average rate for Movie 1: {np.mean(Y[0, R[0,: ] == 1])}\n")

    #  Reduce the data set size so that this runs faster
    num_users = 4
    num_movies = 5
    num_features = 3

    X = X[:num_movies, :num_features]
    theta = theta[:num_users, :num_features]
    Y = Y[:num_movies, 0:num_users]
    R = R[:num_movies, 0:num_users]

    #  Evaluate cost function
    J, grad = cost_function(np.concatenate([X.ravel(), theta.ravel()]),
                            Y, R, num_users, num_movies, num_features)

    print(f"Cost function with loaded parameters: {J}\n")

    #  Evaluate cost function with regularisation
    J, grad = cost_function(np.concatenate([X.ravel(), theta.ravel()]),
                            Y, R, num_users, num_movies, num_features,
                            lambda_=1.5)

    print(f"Cost function and regularisation with loaded parameters: {J}\n")

    movie_list = load_movie_list()
    n_m = len(movie_list)

    my_ratings = np.zeros(n_m)
    my_ratings[0] = 4
    my_ratings[97] = 2
    my_ratings[6] = 3
    my_ratings[11] = 5
    my_ratings[53] = 4
    my_ratings[63] = 5
    my_ratings[65] = 3
    my_ratings[68] = 5
    my_ratings[182] = 4
    my_ratings[225] = 5
    my_ratings[354] = 5

    print("New user ratings:\n")

    for i in range(len(my_ratings)):
        if my_ratings[i] > 0:
            print(f"Rated {my_ratings[i]} stars: {movie_list[i]}")

    data = loadmat("ex8_movies.mat")
    Y, R = data['Y'], data['R']

    Y = np.hstack([my_ratings[:, None], Y])
    R = np.hstack([(my_ratings > 0)[:, None], R])

    Y_norm, Y_mean = normalise_ratings(Y, R)

    num_movies, num_users = Y.shape
    num_features = 10

    X = np.random.randn(num_movies, num_features)
    theta = np.random.randn(num_users, num_features)
    initial_parameters = np.concatenate([X.ravel(), theta.ravel()])

    options = {'maxiter': 100}
    lambda_ = 10
    res = optimize.minimize(lambda x: cost_function(x, Y_norm, R, num_users,
                                                    num_movies, num_features,
                                                    lambda_),
                            initial_parameters,
                            method='TNC',
                            jac=True,
                            options=options)
    theta_model = res.x

    X = theta_model[:num_movies*num_features].reshape(num_movies, num_features)
    theta = theta_model[num_movies*num_features:].reshape(num_users, num_features)

    print("\nRecommender system learning completed.\n")

    p = np.dot(X, theta.T)
    my_predictions = p[:, 0] + Y_mean

    ix = np.argsort(my_predictions)[::-1]

    print("Top recommendations for you:\n")
    for i in range(10):
        j = ix[i]
        print(f"Predicting rating {my_predictions[j]} for movie {movie_list[j]}")
