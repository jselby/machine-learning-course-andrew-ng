#!/usr/bin/env python
# encoding: utf-8

import numpy as np
import re
from scipy.io import loadmat
from sklearn import svm
import matplotlib.pyplot as plt
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize


def gaussian_kernel(x1, x2, sigma):
    vec_sum = np.sum((x1 - x2)**2)
    sim = np.exp(-(vec_sum) / (2 * sigma**2))
    return sim


def dataset3_params(X, y, Xval, yval):
    C_vec = [0.01, 0.03, 0.1, 0.3, 1, 3, 10, 30]
    gamma_vec = [0.01, 0.03, 0.1, 0.3, 1, 3, 10, 30]
    best_error = np.inf
    best_C = 0
    best_gamma = 0

    for C in C_vec:
        for gamma in gamma_vec:
            clf = svm.SVC(C=C, kernel='rbf', gamma=gamma)
            clf.fit(X, y)
            predictions = clf.predict(Xval)
            error = np.mean(predictions != yval)
            if error < best_error:
                best_error = error
                best_C = C
                best_gamma = gamma

    print(f"Optimal settings:\n C: {best_C}\n Gamma: {gamma}\n Error: {error}\n")
    return best_C, best_gamma


def plot_data(X, y, title):
    pos = y == 1
    neg = y == 0

    plt.plot(X[pos, 0], X[pos, 1], '+', mec='k')
    plt.plot(X[neg, 0], X[neg, 1], 'o', mec='y', mew=1, mfc='y')
    plt.title(title)
    plt.show()


def plot_model(X, y, model, title, decision_boundaries=False):
    pos = y == 1
    neg = y == 0

    plt.plot(X[pos, 0], X[pos, 1], '+', mec='k')
    plt.plot(X[neg, 0], X[neg, 1], 'o', mec='y', mew=1, mfc='y')

    clf = model.fit(X, y)
    ax = plt.gca()
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()
    xx, yy = np.meshgrid(np.linspace(xlim[0], xlim[1], 50),
                         np.linspace(ylim[0], ylim[1], 50))
    Z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)

    if decision_boundaries:
        plt.contour(xx, yy, Z, colors='k', levels=[-1, 0, 1], alpha=0.5,
                    linestyles=['--', '-', '--'])
    else:
        plt.contour(xx, yy, Z, colors='k', levels=[0], alpha=0.5,
                    linestyles=['-'])

    plt.title(title)
    plt.show()


def process_email(email_contents):
    word_indices = []
    vocab_dict = get_vocab_list("vocab.txt")

    # Regex contents
    email_contents = email_contents.lower()
    email_contents = re.sub('<[^<>]+>', ' ', email_contents)
    email_contents = re.sub('(http|https)://[^\s]*', 'httpaddr', email_contents)
    email_contents = re.sub('[^\s]+@[^\s]*', 'emailaddr', email_contents)
    email_contents = re.sub('[0-9]+', 'number ', email_contents)
    email_contents = re.sub('[$]+', 'dollar ', email_contents)
    email_contents = re.sub('[^A-Za-z0-9]+', ' ', email_contents)

    # Word stemming and key convert
    ps = PorterStemmer()
    word_tokens = word_tokenize(email_contents)
    for w in word_tokens:
        for key, value in vocab_dict.items():
            if ps.stem(w) == value:
                word_indices.append(key)

        if len(w) < 1:
            continue

    return word_indices


def get_vocab_list(filename):
    vocab_dict = {}

    with open(filename, 'r') as f:
        for line in f:
            (key, val) = line.split()
            vocab_dict[int(key)] = val
    return vocab_dict


def email_features(word_indices):
    vocab_dict = get_vocab_list("vocab.txt")
    X = np.zeros(len(vocab_dict))
    ones = 0

    for key in word_indices:
        X[key-1] = 1

    unique, counts = np.unique(X, return_counts=True)
    ones = dict(zip(unique, counts))[1]

    return X, ones


def svm_predict(X, y, model):
    predictions = model.predict(X)
    accuracy = np.mean(predictions == y) * 100
    return predictions, accuracy


if __name__ == "__main__":

    print("---------------------------")
    print("- Support vector machines -")
    print("---------------------------")
    print("\n")

    # Import data
    data = loadmat("ex6data1.mat")
    X, y = data['X'], data['y'][:, 0]
    print("-- Dataset 1 --\n")

    # Plot data
    plot_data(X, y, "Dataset 1")

    # Train linear svm
    C = 1
    lin_clf = svm.SVC(C=C, kernel='linear')
    plot_model(X, y, lin_clf, f"Linear SVM - C = {C}", decision_boundaries=True)

    C = 100
    lin_clf = svm.SVC(C=C, kernel='linear')
    plot_model(X, y, lin_clf, f"Linear SVM - C = {C}, decision_boundaries=True")

    # Gaussian kernel function
    x1 = np.array([1, 2, 1])
    x2 = np.array([0, 4, -1])
    sigma = 2

    sim = gaussian_kernel(x1, x2, sigma)

    print(f"Gaussian kernel: \n x1: {x1} \n x2: {x2} \n sigma {sigma} \
          \n returns: {sim}\n")

    # Import data
    data = loadmat("ex6data2.mat")
    X, y = data['X'], data['y'][:, 0]
    print("-- Dataset 2 --\n")

    # Plot data
    plot_data(X, y, "Dataset 2")

    # Train svm
    C = 1
    gamma = 30

    clf = svm.SVC(C=C, kernel='rbf', gamma=gamma)
    plot_model(X, y, clf, f"RBF SVM - C: {C} - Gamma: {gamma}")

    # Import data
    data = loadmat("ex6data3.mat")
    X, y, Xval, yval = data['X'], data['y'][:, 0], data['Xval'], \
        data['yval'][:, 0]

    print("-- Dataset 3 --\n")

    # Plot data
    plot_data(X, y, "Dataset 3")

    # Train svm
    C, gamma = dataset3_params(X, y, Xval, yval)
    clf = svm.SVC(C=C, kernel='rbf', gamma=gamma)
    plot_model(X, y, clf, f"RBF SVM - C: {C} - Gamma: {gamma}")

    print("-------------------")
    print("- Spam classifier -")
    print("-------------------")
    print("\n")

    with open('emailSample1.txt', 'r') as file:
        file_contents = file.read()

    word_indices = process_email(file_contents)
    print(f"Sample file:\n\n{file_contents} \nConverted to: \n\n{word_indices} \n")

    X, ones = email_features(word_indices)
    print(f"Extracting features from sample email. Array has {ones} non zero entries.\n")

    # Import data
    data = loadmat("spamTrain.mat")
    X, y = data['X'], data['y'][:, 0]

    data = loadmat("spamTest.mat")
    Xtest, ytest = data['Xtest'], data['ytest'][:, 0]

    print("-- Training spam classifier --\n")
    C = 0.1
    clf_spam = svm.SVC(C=C, kernel='linear')
    clf_spam.fit(X, y)
    p_train, accuracy_train = svm_predict(X, y, clf_spam)
    p_test, accuracy_test = svm_predict(Xtest, ytest, clf_spam)

    print(f"Training accuracy is: {accuracy_train}\nTest accuracy is: {accuracy_test}")
