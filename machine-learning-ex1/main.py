#!/usr/bin/env python
# encoding: utf-8

import numpy as np
import matplotlib.pyplot as plt

def feature_normalisation(X):
    mu = np.mean(X, axis = 0)
    sigma = np.std(X, axis = 0, ddof=1)
    X_n = (X - mu) / sigma
    return(X_n, mu, sigma)

def cost_function(X, y, theta):
    m = y.shape[0]
    error = (X @ theta) - y
    J = 1 / (2*m) * sum(error**2)
    return(J)

def gradient_descent(X, y, theta, alpha, iterations):
    m = y.shape[0]
    J_history = []

    for i in range(iterations):
        J_history.append(cost_function(X, y, theta))
        error = np.transpose(X) @ ((X @ theta) - y)
        theta = theta - (alpha / m) * (error)
    return(theta, J_history)

def normal_equation(X, y):
    theta = np.linalg.inv(np.transpose(X) @ X) @ np.transpose(X) @ y
    return(theta)

def plot_gradient_convergence(J_history):
    J_array = np.asarray(J_history)
    plt.plot(J_array)
    plt.xlabel("Iteration")
    plt.ylabel("$J(\Theta)$")
    plt.title("Cost function using Gradient Descent")
    plt.show()

def predict_y(theta, x, mu, sigma):
    if any(v is None for v in [mu, sigma]):
        x_1 = np.insert(x, 0, 1)
        y = x_1 @ theta
    else:
        x_n = np.transpose((x - mu) / sigma)
        x_n1 = np.insert(x_n, 0, 1)
        y = x_n1 @ theta
    return(y)

def import_data(filename, normalise):
    data = np.loadtxt(filename, delimiter=",")
    y = (data[:,-1])
    X = (data[:,:-1])
    mu = None
    sigma = None

    if normalise == True:
        X, mu, sigma = feature_normalisation(X)

    X_1 = np.c_[np.ones(X.shape[0]),X]
    features = X_1.shape[1]

    return(X_1, y, mu, sigma, features)


if __name__ == "__main__":

    print("---------------------")
    print("- Linear Regression -")
    print("---------------------")
    print("\n")
    print("-- Univariate example -- \n")

    X_1, y, mu, sigma, features = import_data("ex1data1.txt", False)
    theta_initial = np.zeros(features)
    theta_gd, J_history = gradient_descent(X_1, y, theta_initial, alpha=0.01, iterations=1500)
    theta_ne = normal_equation(X_1, y)

    print("Plot of gradient descent convergence:\n")

    plot_gradient_convergence(J_history)

    print(f"Theta calculated from gradient descent: \n{theta_gd}\n")
    print(f"Theta calculated from normal equation: \n{theta_ne}\n")

    predict1 = [3.5]
    predict2 = [7]

    print("\n")
    y_gd = predict_y(theta_gd, predict1, mu, sigma)
    print(f"y prediction from gradient descent: \n{predict1} => {y_gd}\n")
    y_ne= predict_y(theta_ne, predict1, mu, sigma)
    print(f"y prediction from normal equation: \n{predict1} => {y_ne}\n")

    print("\n")
    y_gd = predict_y(theta_gd, predict2, mu, sigma)
    print(f"y prediction from gradient descent: \n{predict2} => {y_gd}\n")
    y_ne= predict_y(theta_ne, predict2, mu, sigma)
    print(f"y prediction from normal equation: \n{predict2} => {y_ne}\n")

# Multivariante example

    print("-- Multivariate example --\n")

    X_1, y, mu, sigma, features = import_data("ex1data2.txt", normalise=True)
    theta_initial = np.zeros(features)
    theta_gd, J_history = gradient_descent(X_1, y, theta_initial, alpha=0.1, iterations=50)
    theta_ne = normal_equation(X_1, y)

    print("Plot of gradient descent convergence:\n")

    plot_gradient_convergence(J_history)

    print(f"Theta calculated from gradient descent: \n{theta_gd}\n")
    print(f"Theta calculated from normal equation: \n{theta_ne}\n")

    predict3 = [1650, 3]

    y_gd = predict_y(theta_gd, predict3, mu, sigma)
    print(f"y prediction from gradient descent: \n{predict3} => {y_gd}\n")
    y_ne= predict_y(theta_ne, predict3, mu, sigma)
    print(f"y prediction from normal equation: \n{predict3} => {y_ne}\n")
