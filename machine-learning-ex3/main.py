#!/usr/bin/env python
# encoding: utf-8

import numpy as np
from scipy import optimize
from scipy.io import loadmat

def import_data(filename):
    data = loadmat(filename)
    X, y = data['X'], data['y'].ravel()

    y[y == 10] = 0

    X_1 = np.c_[np.ones(X.shape[0]), X]
    features = X_1.shape[1]

    return(X_1, y, features)

def import_weights(filename):
    data = loadmat(filename)
    theta_1, theta_2 = data['Theta1'], data['Theta2']
    theta_2 = np.roll(theta_2, 1, axis=0)

    return(theta_1, theta_2)

def cost_function(theta, X_1, y, lambda_=0):
    m = y.shape[0]
    h = sigmoid(X_1 @ theta)

    # cost function
    error = ((-1 * y) * np.log(h)) - (1 - y) * np.log(1 - h)

    regularise_cost = (lambda_/(2*m)) * np.sum(theta[1:]**2)

    J = 1/m * np.sum(error) + regularise_cost

    # gradient
    regularise_grad = (lambda_/m) * theta[1:]

    grad_0 = 1/m * (np.transpose(X_1) @ (h - y))[0]
    grad_1 = 1/m * (np.transpose(X_1) @ (h - y))[1:] + regularise_grad

    grad = 1/m * (np.transpose(X_1) @ (h - y))

    grad = np.hstack([grad_0.reshape(1), grad_1])

    return(J, grad)


def one_vs_all(X_1, y, num_labels, lambda_):
    m, n = X_1.shape
    theta_initial = np.zeros(n)
    theta_all = np.empty([num_labels, n])

    options = {'maxiter': 50}

    for i in range(num_labels):
        # Run minimize to obtain the optimal theta
        res = optimize.minimize(cost_function,
                                theta_initial,
                                (X_1, (y == i), lambda_),
                                jac=True,
                                method='TNC',
                                options=options)
        print(f"For nummber: {i} - Cost Function {res.fun}")
        theta_all[i] = res.x

    return(theta_all)


def sigmoid(z):
    g = 1 / (1 + (np.exp(-z)))
    return(g)

def predict_one_vs_all(theta, X_1):
    p = sigmoid(X_1 @ np.transpose(theta))
    p = np.argmax(p, axis=1)
    return(p)

def predict_nn(theta_1, theta_2, X_1):
    a_2 = sigmoid(X_1 @ np.transpose(theta_1))
    a_2 = np.c_[np.ones(a_2.shape[0]), a_2]
    a_3 = sigmoid(a_2 @ np.transpose(theta_2))
    p = np.argmax(a_3, axis=1)
    return(p)


if __name__ == "__main__":

    print("-----------------------")
    print("- Logistic Regression -")
    print("-----------------------")
    print("\n")

    # import data
    X_1, y, features = import_data("ex3data1.mat")

    # Test cost and gradient
    theta_t = np.array([-2, -1, 1, 2], dtype=float)
    X_t = np.concatenate([np.ones((5, 1)), np.arange(1, 16).reshape(5, 3, order='F')/10.0], axis=1)
    y_t = np.array([1, 0, 1, 0, 1])
    lambda_t = 3

    J, grad = cost_function(theta_t, X_t, y_t, lambda_t)
    print(f"With theta {theta_t}: \n cost function {J} \n gradient {grad} \n")

    # One vs All Prediction
    lambda_ = 0.1
    num_labels = 10
    theta_all = one_vs_all(X_1, y, num_labels, lambda_)

    prediction = predict_one_vs_all(theta_all, X_1)
    accuracy = np.mean(prediction == y) * 100
    print(f"\nAccuracy of model: {accuracy} \n")

    print("-------------------------------------------")
    print("- Neural Network Forward Propagation      -")
    print("-------------------------------------------")
    print("\n")

    theta_1, theta_2 = import_weights("ex3weights.mat")

    prediction = predict_nn(theta_1, theta_2, X_1)
    accuracy = np.mean(prediction == y) * 100
    print(f"\nAccuracy of model: {accuracy} \n")


