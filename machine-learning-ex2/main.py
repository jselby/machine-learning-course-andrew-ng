#!/usr/bin/env python
# encoding: utf-8

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize


def import_data(filename):
    data = np.loadtxt(filename, delimiter=",")
    y = (data[:, -1])
    X = (data[:, :-1])

    X_1 = np.c_[np.ones(X.shape[0]), X]
    features = X_1.shape[1]

    return(X_1, y, features)


def cost_function(theta, X, y, lambda_=0):

    m = y.shape[0]
    h = sigmoid(X @ theta)

    # cost function
    error = (-y * np.log(h)) - (1 - y) * np.log(1 - h)

    regularise = (lambda_/(2*m)) * np.sum(theta[1:]**2)

    J = 1/m * np.sum(error) + regularise

    # gradient

    regularise = (lambda_/m) * theta[1:]

    grad_0 = 1/m * (np.transpose(X) @ (h - y))[0]
    grad_1 = 1/m * (np.transpose(X) @ (h - y))[1:] + regularise

    grad = 1/m * (np.transpose(X) @ (h - y))

    grad = np.hstack([grad_0.reshape(1), grad_1])

    return(J, grad)


def plot_data(X, y, x_label, y_label, title, legend):

    X = (X_1[:, 1:])

    pos = y == 1
    neg = y == 0

    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)

    plt.plot(X[pos, 0], X[pos, 1], 'k+', lw=2, ms=5)
    plt.plot(X[neg, 0], X[neg, 1], 'ko', ms=5, mfc='w', mew=1)

    plt.legend(legend, loc="upper right")
    plt.show()


def sigmoid(z):
    g = 1 / (1 + (np.exp(-z)))
    return(g)


def predict_y(theta, X):
    y = np.round(sigmoid(X @ theta))
    return(y)


def map_feature(X1, X2, degree=6):
    if X1.ndim > 0:
        out = [np.ones(X1.shape[0])]
    else:
        out = [np.ones(1)]

    for i in range(1, degree + 1):
        for j in range(i + 1):
            out.append((X1 ** (i - j)) * (X2 ** j))

    if X1.ndim > 0:
        return np.stack(out, axis=1)
    else:
        return np.array(out)


if __name__ == "__main__":

    print("-----------------------")
    print("- Logistic Regression -")
    print("-----------------------")
    print("\n")

    # import data
    X_1, y, features = import_data("ex2data1.txt")
    theta_zeros = np.zeros(features)

    # plot scatter graph
    plot_data(X_1, y, "Exam Score 1", "Exam Score 2", "Scatter plot of Data", ["Admitted", "Not Admitted"])

    # test cost function and gradient function
    J, grad = cost_function(theta_zeros, X_1, y)
    print(f"With theta {theta_zeros}: \n cost function {J} \n gradient {grad}i \n")

    theta_test = np.array([-24, 0.2, 0.2])
    J, grad = cost_function(theta_test, X_1, y)
    print(f"With theta {theta_test}: \n cost function {J} \n gradient {grad} \n")

    # Apply scipy optimize minimize
    options = {'maxiter': 400}
    res = minimize(cost_function, theta_zeros, (X_1, y), jac=True, method="TNC", options=options)

    cost = res.fun
    theta = res.x
    print(f"SciPy Optimise Minimize: \n Cost function {cost} \n Theta {theta} \n")

    # Evaluation of model
    y_pred = predict_y(theta, X_1)
    y_accuracy = np.mean(y_pred == y) * 100
    print(f"Accuracy of model: {y_accuracy} \n")


    print("-------------------------------------------")
    print("- Logistic Regression with regularisation -")
    print("-------------------------------------------")
    print("\n")

    # import data
    X_1, y, features = import_data("ex2data2.txt")

    # plot data
    plot_data(X_1, y, "Microchip Test 1", "Microchip Test 2", "Scatter plot of Data", ["y = 1", "y = 0"])

    # map feature to polynomial

    X_1 = map_feature(X_1[:, 1], X_1[:, 2])

    theta_zeros = np.zeros(X_1.shape[1])
    J, grad = cost_function(theta_zeros, X_1, y, lambda_=1)
    print(f"With theta {theta_zeros}: \n cost function {J} \n gradient {grad} \n")

    # Apply scipy optimize minimize
    lamda_ = 1
    options = {'maxiter': 400}
    res = minimize(cost_function, theta_zeros, (X_1, y, lamda_), jac=True, method="TNC", options=options)

    cost = res.fun
    theta = res.x
    print(f"SciPy Optimise Minimize: \n Cost function {cost} \n Theta {theta} \n")

    # Evaluation of model
    y_pred = predict_y(theta, X_1)
    y_accuracy = np.mean(y_pred == y) * 100
    print(f"Accuracy of model: {y_accuracy} \n")
