#!/usr/bin/env python
# encoding: utf-8

import numpy as np
from scipy.io import loadmat
import matplotlib.pyplot as plt
import matplotlib as mpl
from tqdm import tqdm


def find_closest_centroid(X, centroids):
    K = centroids.shape[0]
    idx = np.empty(X.shape[0])

    for i in range(X.shape[0]):
        min_length = np.inf

        for j in range(K):
            length = np.sum((X[i] - centroids[j]) ** 2)
            if length < min_length:
                min_length = length
                idx[i] = j

    return idx.astype(int)


def compute_centroid_mean(X, idx, K):
    m, n = X.shape
    centroids = np.empty((K, n))

    for k in range(K):
        X_k = np.delete(X, np.argwhere(idx != k), axis=0)
        centroids[k] = np.sum(X_k, axis=0) / X_k.shape[0]

    return centroids


def run_k_means(X, centroids, max_iters, plot=False):
    K = centroids.shape[0]
    idx_history = []
    centroids_history = []

    for i in tqdm(range(max_iters)):
        idx = find_closest_centroid(X, centroids)
        centroids = compute_centroid_mean(X, idx, K)

        if plot:
            idx_history.append(idx)
            centroids_history.append(centroids)

    if plot:
        plot_k_means(X, centroids_history, idx_history)

    return centroids, idx


def k_means_initial_centroid(X, K):
    randidx = np.random.permutation(X.shape[0])
    centroids = X[randidx[:K], :]
    return centroids


def pca(X):
    m, n = X.shape
    sigma = (np.transpose(X) @ X) / m
    U, S, V = np.linalg.svd(sigma)
    return U, S


def project_data(X, U, K):
    U_reduce = U[:, :K]
    Z = X @ U_reduce
    return Z


def recover_data(Z, U, K):
    v = Z[:, :K]
    U_reduce = U[:, :K]
    X_rec = v @ np.transpose(U_reduce)
    return X_rec


def feature_normalisation(X):
    mu = np.mean(X, axis=0)
    sigma = np.std(X, axis=0, ddof=1)
    X_norm = (X - mu) / sigma
    return X_norm, mu, sigma


def plot_k_means(X, centroids_history, idx_history):

    K = centroids_history[0].shape[0]
    iters = len(centroids_history)

    for k in range(K):
        k_list = [item[k] for item in centroids_history]
        k_x1 = [item[0] for item in k_list]
        k_x2 = [item[1] for item in k_list]

        plt.plot(k_x1, k_x2, 'x--', linewidth=2, markersize=5)

    plt.scatter(X[:, 0], X[:, 1], s=2, c='k')
    plt.title(f"Iterations: {iters}")
    plt.show()


def plot_images(A, X, K):
    fig, ax = plt.subplots(1, 2, figsize=(8, 4))
    ax[0].imshow(A*255)
    ax[0].set_title('Original')
    ax[0].grid(False)

    ax[1].imshow(X*255)
    ax[1].set_title(f"Compressed, with {K} colors")
    ax[1].grid(False)
    plt.show()


def plot_pca(X, title, plot_eigenvectors=False, mu=0, U=0, S=0):
    fig, ax = plt.subplots()
    ax.plot(X[:, 0], X[:, 1], 'o', ms=5, mec='k', mew=1)

    if plot_eigenvectors:
        for i in range(2):
            ax.arrow(mu[0], mu[1], 1.5 * S[i]*U[0, i], 1.5 * S[i]*U[1, i],
                     head_width=0.25, head_length=0.2, fc='k', ec='k',
                     lw=2, zorder=1000)

    ax.grid(False)
    plt.title(title)
    plt.show()


def display_data(X, example_width=None, figsize=(10, 10)):

    # Compute rows, cols
    if X.ndim == 2:
        m, n = X.shape
    elif X.ndim == 1:
        n = X.size
        m = 1
        X = X[None]  # Promote to a 2 dimensional array
    else:
        raise IndexError('Input X should be 1 or 2 dimensional.')

    example_width = example_width or int(np.round(np.sqrt(n)))
    example_height = int(n / example_width)

    # Compute number of items to display
    display_rows = int(np.floor(np.sqrt(m)))
    display_cols = int(np.ceil(m / display_rows))

    fig, ax_array = plt.subplots(display_rows, display_cols, figsize=figsize)
    fig.subplots_adjust(wspace=0.025, hspace=0.025)

    ax_array = [ax_array] if m == 1 else ax_array.ravel()

    for i, ax in enumerate(ax_array):
        ax.imshow(X[i].reshape(example_height, example_width, order='F'),
                  cmap='gray')
        ax.axis('off')

    plt.show()


if __name__ == "__main__":

    print("----------------------")
    print("- K-means clustering -")
    print("----------------------")
    print("\n")

    # Import data
    data = loadmat("ex7data2.mat")
    X = data['X']

    # Find closest centroid
    K = 3
    initial_centroids = np.array([[3, 3], [6, 2], [8, 5]])

    idx = find_closest_centroid(X, initial_centroids)

    print(f"Closest centroids for first 3 examples:\n {idx[:3]}\n")

    # Compute centroid mean
    centroids = compute_centroid_mean(X, idx, K)

    print(f"Centroids found after initial finding:\n {centroids}\n")

    # K-means on example dataset
    K = 3
    max_iters = 10
    centroids, idx = run_k_means(X, initial_centroids, max_iters, plot=True)

    # Randomise initial centroids
    initial_centroids = k_means_initial_centroid(X, K)

    # K-means on example dataset
    K = 3
    max_iters = 10
    centroids, idx = run_k_means(X, initial_centroids, max_iters, plot=True)

    # Image compression
    print("-- Image compression --\n")

    A = mpl.image.imread('bird_small.png')
    K = 16
    max_iters = 10

    print(f"With K: {K}; Iterations: {max_iters}\n")

    A /= 255
    X = A.reshape(-1, 3)

    initial_centroids = k_means_initial_centroid(X, K)
    centroids, idx = run_k_means(X, initial_centroids, max_iters)

    X_recovered = centroids[idx, :].reshape(A.shape)

    plot_images(A, X_recovered, K)

    print("\n--------------------------------")
    print("- Principal Component Analysis -")
    print("--------------------------------")
    print("\n")

    # Import data
    data = loadmat("ex7data1.mat")
    X = data['X']

    plot_pca(X, "Dataset for PCA")

    # Feature normalise
    X_norm, mu, sigma = feature_normalisation(X)

    # Run PCA
    U, S = pca(X_norm)

    plot_pca(X, "Dataset for PCA", True, mu, U, S)

    print(f"Top eigenvector: {U[0, 0]} {U[1, 0]} \n")

    # Project data onto K = 1 dimension
    K = 1
    Z = project_data(X_norm, U, K)

    print(f"Projected first example: {Z[0, 0]} \n")

    # Reconstruct approximation of data
    X_rec = recover_data(Z, U, K)
    print(f"Approximation of the first example: {X_rec[0, 0]} {X_rec[0, 1]}\n")

    # Face image dataset
    print("-- PCA on faces dataset --\n")

    # Import data
    data = loadmat("ex7faces.mat")
    X = data['X']

    print("Original Faces\n")
    display_data(X[:100, :], figsize=(8, 8))

    # Feature normalise
    X_norm, mu, sigma = feature_normalisation(X)

    # Run PCA
    U, S = pca(X_norm)

    print("Eigenvectors\n")
    display_data(U[:, :36].T, figsize=(8, 8))

    # Project data onto K = 100 dimension
    K = 100
    Z = project_data(X_norm, U, K)

    # Reconstruct approximation of data
    X_rec = recover_data(Z, U, K)

    print("Normalised faces\n")
    display_data(X_norm[:100, :], figsize=(6, 6))

    print("Recovered faces\n")
    display_data(X_rec[:100, :], figsize=(6, 6))
