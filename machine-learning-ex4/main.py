#!/usr/bin/env python
# encoding: utf-8

import numpy as np
from scipy import optimize
from scipy.io import loadmat


def import_data(filename):
    data = loadmat(filename)
    X, y = data['X'], data['y'].ravel()
    y[y == 10] = 0
    return(X, y)


def import_weights(filename):
    data = loadmat(filename)
    theta_1, theta_2 = data['Theta1'], data['Theta2']
    theta_2 = np.roll(theta_2, 1, axis=0)
    nn_params = np.concatenate([theta_1.ravel(), theta_2.ravel()])
    return(nn_params)


def cost_function(nn_params, input_layer_size, hidden_layer_size, num_labels,
                  X, y, lambda_=0):

    theta_1 = np.reshape(nn_params[:hidden_layer_size * (input_layer_size + 1)],
                         (hidden_layer_size, (input_layer_size + 1)))
    theta_2 = np.reshape(nn_params[(hidden_layer_size * (input_layer_size + 1)):],
                         (num_labels, (hidden_layer_size + 1)))
    m = y.shape[0]

    # forward propagation
    a_1 = np.c_[np.ones(X.shape[0]), X]
    z_2 = a_1 @ np.transpose(theta_1)
    a_2 = sigmoid(z_2)
    a_2 = np.c_[np.ones(a_2.shape[0]), a_2]
    z_3 = a_2 @ np.transpose(theta_2)
    a_3 = sigmoid(z_3)

    # convert y to dummies
    y_v = np.zeros([m, num_labels])
    y_v[np.arange(m), y] = 1

    # cost function
    error = ((-1 * y_v) * np.log(a_3)) - (1 - y_v) * np.log(1 - a_3)
    regularise_cost = (lambda_/(2*m)) * (np.sum(np.sum(theta_1[:, 1:]**2))
                                         + np.sum(np.sum(theta_2[:, 1:]**2)))
    J = 1/m * np.sum(np.sum(error)) + regularise_cost

    # backward propagation and gradient regularisation
    d_3 = a_3 - y_v
    gz_2 = sigmoid_gradient(np.c_[np.ones(z_2.shape[0]), z_2])
    d_2 = (d_3 @ theta_2) * gz_2
    d_2 = d_2[:, 1:]

    theta_1_grad = np.zeros(theta_1.shape)
    theta_1_grad += (np.transpose(d_2) @ a_1)
    nn_theta_1_grad = theta_1_grad/m + (lambda_/m) \
        * np.column_stack((np.zeros(theta_1.shape[0]), theta_1[:, 1:]))

    theta_2_grad = np.zeros(theta_2.shape)
    theta_2_grad += (np.transpose(d_3) @ a_2)
    nn_theta_2_grad = theta_2_grad/m + (lambda_/m) \
        * np.column_stack((np.zeros(theta_2.shape[0]), theta_2[:, 1:]))

    grad = np.concatenate([nn_theta_1_grad.ravel(), nn_theta_2_grad.ravel()])

    return(J, grad)


def sigmoid(z):
    g = 1 / (1 + (np.exp(-z)))
    return(g)


def sigmoid_gradient(z):
    g = sigmoid(z) * (1 - sigmoid(z))
    return(g)


def rand_init_weights(L_in, L_out, episilon_init=0.12):
    W = np.random.rand(L_out, 1 + L_in) * 2 * episilon_init - episilon_init
    return(W)


def predict_nn(theta_1, theta_2, X):
    a_1 = np.c_[np.ones(X.shape[0]), X]
    a_2 = sigmoid(a_1 @ np.transpose(theta_1))
    a_2 = np.c_[np.ones(a_2.shape[0]), a_2]
    a_3 = sigmoid(a_2 @ np.transpose(theta_2))
    p = np.argmax(a_3, axis=1)
    return(p)


def debug_initialize_weights(fan_out, fan_in):
    W = np.sin(np.arange(1, 1 + (1+fan_in)*fan_out))/10
    W = W.reshape(fan_out, 1+fan_in, order='F')
    return(W)


def check_gradients(cost_function, lambda_=0):
    input_layer_size = 3
    hidden_layer_size = 5
    num_labels = 3
    m = 5

    theta_1 = debug_initialize_weights(hidden_layer_size, input_layer_size)
    theta_2 = debug_initialize_weights(num_labels, hidden_layer_size)
    nn_params = np.concatenate([theta_1.ravel(), theta_2.ravel()])
    X = debug_initialize_weights(m, input_layer_size - 1)
    y = np.arange(1, 1+m) % num_labels

    cost_func = lambda p: cost_function(p, input_layer_size, hidden_layer_size,
                                        num_labels, X, y, lambda_)
    J, grad = cost_func(nn_params)
    numgrad = compute_numerical_gradient(cost_func, nn_params)

    print("\nGradient Checking:")
    print(np.stack([numgrad, grad], axis=1))
    diff = np.linalg.norm(numgrad - grad)/np.linalg.norm(numgrad + grad)
    print(f"\nRelative Difference: \n {diff} \n")


def compute_numerical_gradient(J, theta, e=1e-4):
    numgrad = np.zeros(theta.shape)
    perturb = np.diag(e * np.ones(theta.shape))
    for i in range(theta.size):
        loss1, _ = J(theta - perturb[:, i])
        loss2, _ = J(theta + perturb[:, i])
        numgrad[i] = (loss2 - loss1)/(2*e)
    return numgrad


if __name__ == "__main__":

    print("-----------------------")
    print("-   Neural Networks   -")
    print("-----------------------")
    print("\n")

    # import data
    X, y = import_data("ex4data1.mat")
    nn_params = import_weights("ex4weights.mat")

    # set variables
    input_layer_size = 400   # 20x20 Input Images of Digits
    hidden_layer_size = 25   # 25 hidden units
    num_labels = 10          # 10 labels, from 0 to 9
    lambda_ = 0

    theta_1_rand = rand_init_weights(input_layer_size, hidden_layer_size)
    theta_2_rand = rand_init_weights(hidden_layer_size, num_labels)
    nn_params_rand = np.concatenate([theta_1_rand.ravel(),
                                     theta_2_rand.ravel()], axis=0)

    # Testing cost function
    J, grad = cost_function(nn_params, input_layer_size, hidden_layer_size,
                            num_labels, X, y, lambda_)

    print(f"Cost calculated from ex4weights: \n {J} \n")

    lambda_ = 1
    J, grad = cost_function(nn_params, input_layer_size, hidden_layer_size,
                            num_labels, X, y, lambda_)

    print(f"Cost calculated from ex4weights with lambda {lambda_}: \n {J} \n")

    # Testing gradient function
    check_gradients(cost_function, lambda_=0)
    print("Check gradient with lambda: 3 \n")
    check_gradients(cost_function, lambda_=3)

    # Training the neural network
    print("Training the neural network \n")
    options = {'maxiter': 100}
    lambda_ = 0.1

    cost_func = lambda p: cost_function(p, input_layer_size, hidden_layer_size,
                                        num_labels, X, y, lambda_)

    res = optimize.minimize(cost_func,
                            nn_params_rand,
                            jac=True,
                            method='TNC',
                            options=options)

    nn_params = res.x
    theta_1 = np.reshape(nn_params[:hidden_layer_size * (input_layer_size + 1)],
                         (hidden_layer_size, (input_layer_size + 1)))

    theta_2 = np.reshape(nn_params[(hidden_layer_size * (input_layer_size + 1)):],
                         (num_labels, (hidden_layer_size + 1)))

    # Accuracy
    prediction = predict_nn(theta_1, theta_2, X)
    accuracy = np.mean(prediction == y) * 100
    print(f"Accuracy of model: {accuracy} \n")


