# Machine Learning Course Andrew Ng

# About

Coding assignments, completed in python, from Andrew Ng's Coursera [Machine Learning Course](https://www.coursera.org/learn/machine-learning). This [repository](https://github.com/dibgerge/ml-coursera-python-assignments) was used as a guide to writing the 'non coding assignment' code.

# How to use

1. Clone repository

```
$ git clone https://gitlab.com/jselby/machine-learning-course-andrew-ng
```

2. Install python requirements

```
$ pip install -r requirements.txt
```

3. Run `main.py` from exercise week, e.g:

```
$ cd machine-learning-ex1/
$ python3 main.py
```

