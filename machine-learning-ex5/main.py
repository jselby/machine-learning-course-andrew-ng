#!/usr/bin/env python
# encoding: utf-8

import numpy as np
from scipy import optimize
from scipy.io import loadmat
import matplotlib.pyplot as plt


def cost_function(X, y, theta, lambda_=0):
    m = y.shape[0]
    h = X @ theta
    error = (h - y)

    # cost function
    regularise_cost = (lambda_ / (2*m)) * np.sum(theta[1:]**2)
    J = 1/(2*m) * np.sum(error**2) + regularise_cost

    # gradient
    grad = np.empty(theta.shape)
    grad[0] = 1/m * (error @ X)[0]
    grad[1:] = 1/m * (error @ X)[1:] + (lambda_/m) * theta[1:]

    return J, grad


def learning_curve(X, y, Xval, yval, lambda_=0):
    print("\n- Learning curve -")
    m = y.shape[0]
    error_train = np.zeros(m)
    error_val = np.zeros(m)

    for i in range(1, m+1):
        theta = train_linear_reg(cost_function, X[:i, ], y[:i], lambda_)
        error_train[i-1], _ = cost_function(X[:i, ], y[:i], theta, lambda_=0)
        error_val[i-1], _ = cost_function(Xval, yval, theta, lambda_=0)

    print("Training errors | Validation errors")
    print(np.stack([error_train, error_val], axis=1))
    print("\n")

    plt.plot(np.arange(1, m+1), error_train)
    plt.plot(np.arange(1, m+1), error_val)
    plt.xlabel("Number of training examples")
    plt.ylabel("Error")
    plt.legend(['Train', 'Cross Validation'])
    plt.title("Learning curve for linear regression")
    plt.show()


def poly_features(X, p):
    X_poly = np.zeros((X.shape[0], p))

    for i in range(1, p+1):
        X_poly[:, i-1] = X.flatten()**(i)

    return X_poly


def feature_normalise(X):
    mu = np.mean(X, axis=0)
    sigma = np.std(X, axis=0, ddof=1)
    X_norm = (X - mu) / sigma

    return X_norm, mu, sigma


def plot_data(X, y, xlabel, ylabel, title, theta=0):
    plt.plot(X, y, 'kx', ms=5)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.show()


def plot_data_function(X, y, theta, xlabel, ylabel, title, normalise=False):
    indices = np.argsort(X[:, 0])
    X_1 = np.c_[np.ones(X.shape[0]), X]

    if normalise:
        X_n, _, _ = feature_normalise(X)
        X_1 = np.c_[np.ones(X_n.shape[0]), X_n]

    plt.plot(X[:, 0], y, 'kx', ms=5)
    plt.plot(np.sort(X[:, 0][indices]), (X_1 @ theta)[indices], '--', lw=2)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.show()


def validation_curve(X, y, Xval, yval):
    print("\n- Validation curve -")
    lambda_vec = [0, 0.001, 0.003, 0.01, 0.03, 0.1, 0.3, 1, 3, 10]
    error_train = np.zeros(len(lambda_vec))
    error_val = np.zeros(len(lambda_vec))

    for i in range(len(lambda_vec)):
        lambda_ = lambda_vec[i]
        theta = train_linear_reg(cost_function, X, y, lambda_)
        error_train[i], _ = cost_function(X, y, theta, lambda_=0)
        error_val[i], _ = cost_function(Xval, yval, theta, lambda_=0)

    print("Training errors | Validation errors")
    print(np.stack([error_train, error_val], axis=1))
    print("\n")

    plt.plot(lambda_vec, error_train)
    plt.plot(lambda_vec, error_val)
    plt.xlabel("Lambda")
    plt.ylabel("Error")
    plt.legend(['Train', 'Cross Validation'])
    plt.title("Validation curve for linear regression")
    plt.show()


def train_linear_reg(cost_function, X, y, lambda_=0, maxiter=200):
    theta_zeros = np.zeros(X.shape[1])
    cost_function_lambda = lambda t: cost_function(X, y, t, lambda_)
    options = {'maxiter': maxiter}

    res = optimize.minimize(cost_function_lambda, theta_zeros, jac=True,
                            method='TNC', options=options)
    return res.x


if __name__ == "__main__":

    print("------------------------------------------------------")
    print("- Regularized linear regression and bias vs variance -")
    print("------------------------------------------------------")
    print("\n")

    # Import data
    data = loadmat("ex5data1.mat")
    X, y = data['X'], data['y'][:, 0]
    Xtest, ytest = data['Xtest'], data['ytest'][:, 0]
    Xval, yval = data['Xval'], data['yval'][:, 0]
    X_1 = np.c_[np.ones(X.shape[0]), X]
    Xval_1 = np.c_[np.ones(Xval.shape[0]), Xval]
    Xtest_1 = np.c_[np.ones(Xtest.shape[0]), Xtest]

    # Plot data
    plot_data(X, y, "Change in water level", "Water flowing out of dam",
              "Data")

    # Test cost function
    theta_ones = np.ones(X_1.shape[1])
    lambda_ = 1
    J, grad = cost_function(X_1, y, theta_ones, lambda_)
    print(f"Testing cost function with theta: [1, 1]  and lambda: {lambda_}: \
          \n Cost {J} \n Gradient {grad}\n")

    # Linear regression
    lambda_ = 1
    theta_train = train_linear_reg(cost_function, X_1, y, lambda_)
    print(f"Calculated theta value to fit model: \n {theta_train}")
    plot_data_function(X, y, theta_train, "Change in water level",
                       "Water flowing out of dam", "Data")

    # Learning curve
    learning_curve(X_1, y, Xval_1, yval, lambda_=0)

    # Polynomial regression
    p = 8
    X_poly = poly_features(X, p)
    X_poly_n, mu, sigma = feature_normalise(X_poly)
    X_poly_n_1 = np.c_[np.ones(X_poly_n.shape[0]), X_poly_n]

    X_poly_test = poly_features(Xtest, p)
    X_poly_test -= mu
    X_poly_test /= sigma
    X_poly_test_1 = np.c_[np.ones(X_poly_test.shape[0]), X_poly_test]

    X_poly_val = poly_features(Xval, p)
    X_poly_val -= mu
    X_poly_val /= sigma
    X_poly_val_1 = np.c_[np.ones(X_poly_val.shape[0]), X_poly_val]

    lambda_ = 1
    theta_train = train_linear_reg(cost_function, X_poly_n_1, y,
                                   lambda_, maxiter=55)

    print(f"Theta value from polynomial regression, with lambda {lambda_}:\n \
          {theta_train}")

    plot_data_function(X_poly, y, theta_train, "Change in water lever",
                       "Water flowing out of dam",
                       f"Polynomial regression Fit: Lambda {lambda_}",
                       normalise=True)

    # Learning curve

    learning_curve(X_poly_n_1, y, X_poly_val_1, yval, lambda_)

    # Validation curve
    validation_curve(X_poly_n_1, y, X_poly_val_1, yval)

    # Evaluating error on test data
    lambda_ = 0.9
    theta_train = train_linear_reg(cost_function, X_poly_n_1, y,
                                   lambda_, maxiter=55)
    error_test, _ = cost_function(X_poly_test_1, ytest, theta_train, lambda_)
    print(f"Error on test data set, with lambda {lambda_}:\n {error_test}")
